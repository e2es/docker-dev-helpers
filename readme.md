# dev-helpers

A set of Docker Compose projects to bring up several tools that may help you when developing locally.

The instructions below are specific to this repository and will only give a very small overview of what each project is about. For more information on each, visit their respective project pages.

## Traefik

A proxy that inspects all of the containers you have running in Docker and exposes them as \*.localhost addresses.

To run it:

-   `cd` into the `traefik` directory
-   run `docker network create traefik --attachable`
-   run `docker compose up -d`

You may now browse to `http://traefik.localhost` in your browser to see a list of containers it has detected and made available.

## MinIO

A storage engine that is compatible with AWS S3 and will allow you to upload media files on your local sites without needing to set up S3 itself.

To run it:

-   `cd` into the `minio` directory
-   run `docker compose up -d`

Assuming you have installed Traefik as above, you may now browse to `http://minio.localhost` in your browser to see the MinIO console.

Our dev projects are configured to connect to this instance of MinIO automatically.

## Portainer

A GUI to manage the containers running on your system.

To run it:

-   `cd` into the `portainer` directory
-   run `docker compose up -d`

Assuming you have installed Traefik as above, you may now browse to `http://portainer.localhost` in your browser to see the Portainer console.
